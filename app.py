from chatbot import chatbot
from flask import Flask, render_template
from flask_socketio import SocketIO, send, emit
import datetime
import nltk

app = Flask(__name__)
app.static_folder = 'static'
app.secret_key = b'secret'
socketio = SocketIO(app)


@app.route("/")
def home():
    return render_template("index.html")


@socketio.on('message')
def message(data):
    send(data)
    emit('reply', str(chatbot.get_response(data)))


if __name__ == "__main__":
    socketio.run(app, host='0.0.0.0')
