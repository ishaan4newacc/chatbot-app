document.addEventListener("DOMContentLoaded", () => {
  // Create connection socketIO
  var socket = io.connect("http://" + document.domain + ":" + location.port);

  socket.on("connect", () => {
    appendMessage(botName, botImg, "left", "Hi, ask me anything!");
    // console.log("Connected");

    connectedTime(time);
  });

  // ChatBot Initialize
  const botName = "ChatBot";
  const botImg = "../static/bot.svg";

  // User Initialize
  const userName = "You";
  const userImg = "../static/user.svg";

  // Set a time to disconnect when there is no activity
  let time = 60000; // 60sec
  let timer;

  // Set time to timer
  function connectedTime(time) {
    timer = setTimeout(function (time) {
      appendMessage(botName, botImg, "left", "ZzzZzzZzz");
      socket.disconnect();
      // console.log("Disconnected");

      document.querySelector("#send_message").innerHTML = "Poke";
      document
        .querySelector("#user_message")
        .setAttribute("placeholder", "ChatBot is sleeping, poke him!");
      document
        .querySelector("#user_message")
        .setAttributeNode(document.createAttribute("disabled"));
    }, time);
  }

  // Get Time
  function formatDate(date) {
    const h = "0" + date.getHours();
    const m = "0" + date.getMinutes();

    return `${h.slice(-2)}:${m.slice(-2)}`;
  }

  // Insert Message to Display Message
  function appendMessage(name, img, side, text) {
    const msgHTML = `
      <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>
      
      <div class="msg-bubble">
      <div class="msg-info">
      <div class="msg-info-name">${name}</div>
      <div class="msg-info-time">${formatDate(new Date())}</div>
      </div>
      
      <div class="msg-text">${text}</div>
      </div>
      </div>
      `;

    document
      .querySelector(".msger-chat")
      .insertAdjacentHTML("beforeend", msgHTML);
    document.querySelector(".msger-chat").scrollTop += 500;
  }

  // Send Message
  socket.on("message", (data) => {
    appendMessage(userName, userImg, "right", data);
  });

  // Receive Reply Message from ChatBot
  socket.on("reply", (data) => {
    if (timer != "undefined") {
      clearTimeout(timer);
    }
    appendMessage(botName, botImg, "left", data);
    connectedTime(time);
  });

  // onclick Handler
  document.querySelector("#send_message").onclick = (e) => {
    if (socket.disconnected) {
      socket.connect();

      document.querySelector("#send_message").innerHTML = "Send";
      document
        .querySelector("#user_message")
        .setAttribute("placeholder", "Enter your message...");
      document.querySelector("#user_message").removeAttribute("disabled");
    }
    if (document.querySelector("#user_message").value != "") {
      socket.send(document.querySelector("#user_message").value);
      document.querySelector("#user_message").value = "";
    }
    e.preventDefault();
  };
});
