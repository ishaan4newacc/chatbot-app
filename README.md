# ChatBot-app
https://gitlab.com/caacuk/chatbot-app

![Chatbot](https://github.com/caacuk/chatbot-app/blob/master/Capture.PNG?raw=true)

Chatbot will reply "I am sorry, but I do not understand." if the questions are not in QnA pairs. 
Chatbot disconnected after no activity within 1 minutes.
![Chatbot disconnected after no activity within 1 minutes](https://github.com/caacuk/chatbot-app/blob/master/Capture2.PNG?raw=true)


## Docker

https://hub.docker.com/r/caacuk/chatbot-app



> docker pull caacuk/chatbot-app

> docker run -p 5000:5000 --name caacuk-chatbot-app -it caacuk/chatbot-app
